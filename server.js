const express = require('express')
const fetch = require('node-fetch')
const { verifierFactory } = require('@southlane/cognito-jwt-verifier')

const app = express()
const { appDomain, region, userPoolId, appClientId, appSecret, redirect_uri } = {
  appDomain: '<app_domain>',
  region: '<region>',
  userPoolId: '<user_pool_id>',
  appClientId: '<app_client_id>',
  appSecret: '<app_secret>',
  redirect_uri: 'http://localhost:5000/loggedIn.html'
}

const verifier = verifierFactory({ region, userPoolId, appClientId, tokenType: 'id' })

const tokenUrl = `https://${appDomain}.auth.${region}.amazoncognito.com/oauth2/token`

const getJwt = code => fetch(tokenUrl, {
  method: 'POST',
  body: new URLSearchParams({
    grant_type: 'authorization_code',
    code,
    client_id: appClientId,
    redirect_uri
  }),
  headers: {
    Authorization: 'Basic ' + new Buffer.from(appClientId + ":" + appSecret).toString('base64')
  }
}).then(res => res.json())

app.use(express.static(__dirname + '/public'))

app.get('/token', (req, res) => {
  getJwt(req.query.code).then(data => res.end(JSON.stringify(data)))
})

app.get('/verify', (req, res) => {
  verifier.verify(req.query.token)
    .then(payload => res.end(JSON.stringify({ success: true, payload })))
    .catch(e => res.end(JSON.stringify(e)))
})

app.listen(5000)