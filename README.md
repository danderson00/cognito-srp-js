# cognito-srp-js

Simple Node.js / browser example for authenticating against AWS Cognito using Secure Remote Password and verifying 
tokens.

## Installation

```shell
git clone https://gitlab.com/danderson00/cognito-srp-js.git
cd cognito-srp-js
yarn
```

## Configuration

1) Open the AWS console create a user pool using the "Review defaults" flow
2) Add an app client with the default settings
3) Under "App client settings", select all identity providers, "Authorization code grant" OAuth flow, "openid" and 
   "profile" OAuth scopes and set the callback URL to `http://localhost:5000/loggedIn.html`. The sign out URL can be 
   set to any valid URL (not used in this example)
4) Under "Domain name", enter a name 
5) Create a user in the "Users and groups" section

Open `server.js` and `public/index.html` and replace configuration values with the appropriate values from the "App 
clients" section.

## Usage

```shell
yarn start
```

Open a browser window to `http://localhost:5000`. Clicking the login button will launch the login flow. After 
successfully logging in, clicking the verify button will verify the stored JWT token. Server responses are logged to 
the console.
